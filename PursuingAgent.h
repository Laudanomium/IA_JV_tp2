#ifndef PURSUINGAGENT_H
#define PURSUINGAGENT_H
#pragma warning (disable:4786)
//------------------------------------------------------------------------
//
//  Name:   PursuingAgent.h
//
//  Desc:   Definition of a pursuing agent that uses steering behaviors
//
//  Author: Hugo Brunet
//
//------------------------------------------------------------------------
#include "Vehicle.h"
#include "../Common/2d/Vector2D.h"
#include "../Common/misc/Smoother.h"

#include <vector>
#include <list>
#include <string>

class GameWorld;
class SteeringBehavior;



class PursuingAgent : public Vehicle
{

private:
	Vector2D offset;
	Vehicle* leader;

public: 
	PursuingAgent(GameWorld* world,
		Vector2D position,
		double    rotation,
		Vector2D velocity,
		double    mass,
		double    max_force,
		double    max_speed,
		double    max_turn_rate,
		double    scale,
		Vehicle*  leader,
		Vector2D  offset);

	void SetOffset(Vector2D v);
	void SetLeader(Vehicle* v);
	void UpdatePursuit();
};

#endif
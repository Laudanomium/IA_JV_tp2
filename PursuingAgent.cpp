#include "PursuingAgent.h"
#include "../Common/2d/C2DMatrix.h"
#include "../Common/2d/Geometry.h"
#include "SteeringBehaviors.h"
#include "../Common/2d/Transformations.h"
#include "GameWorld.h"
#include "../Common/misc/CellSpacePartition.h"
#include "../Common/misc/cgdi.h"


using std::vector;
using std::list;

PursuingAgent::PursuingAgent(GameWorld* world,
	Vector2D position,
	double    rotation,
	Vector2D velocity,
	double    mass,
	double    max_force,
	double    max_speed,
	double    max_turn_rate,
	double    scale,
	Vehicle*  leader,
	Vector2D  offset) : Vehicle(world,
		position,
		rotation,
		velocity,
		mass,
		max_force,
		max_speed,
		max_turn_rate,
		scale)
{
	this->Steering()->AlignmentOn();
	this->Steering()->CohesionOn();
	
	this->Steering()->FlockingOn();
	this->SetLeader(leader);
	this->SetOffset(offset);
	this->UpdatePursuit();
}

void PursuingAgent::SetOffset(Vector2D v)
{
	offset = v;
}

void PursuingAgent::SetLeader(Vehicle* v)
{
	this->leader = v;
}

void PursuingAgent::UpdatePursuit()
{
	this->Steering()->OffsetPursuitOff();
	this->Steering()->OffsetPursuitOn(leader, offset);
}
#ifndef LEADERAGENT_H
#define LEADERAGENT_H
#pragma warning (disable:4786)
//------------------------------------------------------------------------
//
//  Name:   LeaderAgent.h
//
//  Desc:   Definition of a leader agent that uses steering behaviors
//
//  Author: Hugo Brunet
//
//------------------------------------------------------------------------
#include "Vehicle.h"
#include "../Common/2d/Vector2D.h"
#include "../Common/misc/Smoother.h"

#include <vector>
#include <list>
#include <string>

class GameWorld;
class SteeringBehavior;



class LeaderAgent : public Vehicle
{
private:
	bool userControl = false;
	Vector2D arrivePos;

public:
	LeaderAgent(GameWorld* world,
		Vector2D  position,
		double    rotation,
		Vector2D  velocity,
		double    mass,
		double    max_force,
		double    max_speed,
		double    max_turn_rate,
		double    scale, 
		bool	  user_control);

	void SwitchMode();
	
	void GoUp();
	void GoDown();
	void GoLeft();
	void GoRight();

	void SetArrivePos(Vector2D v) { arrivePos = v; };
	void SetUserControl(bool b) { userControl = b; };

	void Render();
};

#endif

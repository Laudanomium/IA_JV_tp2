#include "LeaderAgent.h"
#include "../Common/2d/C2DMatrix.h"
#include "../Common/2d/Geometry.h"
#include "SteeringBehaviors.h"
#include "../Common/2d/Transformations.h"
#include "GameWorld.h"
#include "../Common/misc/CellSpacePartition.h"
#include "../Common/misc/cgdi.h"


using std::vector;
using std::list;

LeaderAgent::LeaderAgent(GameWorld* world,
	Vector2D position,
	double    rotation,
	Vector2D velocity,
	double    mass,
	double    max_force,
	double    max_speed,
	double    max_turn_rate,
	double    scale,
	bool	  user_control) : Vehicle(world,
		position,
		rotation,
		velocity,
		mass,
		max_force,
		max_speed,
		max_turn_rate,
		scale)
{
	gdi->GreenPen();
	this->userControl = user_control;
	this->arrivePos = position;

	this->Steering()->FlockingOff();
	this->SetScale(Vector2D(10, 10));
	if (user_control == false)
	{
		this->Steering()->WanderOn();	
	}
	else
	{
		this->World()->SetCrosshair(this->arrivePos);
		this->Steering()->ArriveOn();
	}
	this->SetMaxSpeed(70);
};

void LeaderAgent::SwitchMode()
{
	this->userControl = !(this->userControl);
	if (this->userControl == true)
	{
		this->Steering()->WanderOff();
		this->SetArrivePos(this->Pos());
		this->World()->SetCrosshair(this->arrivePos);
		this->Steering()->ArriveOn();
	}
	else
	{
		this->Steering()->ArriveOff();
		this->Steering()->WanderOn();
	}
}

void LeaderAgent::GoUp()
{
	if (this->userControl)
	{
		
		if (Vec2DDistance(this->arrivePos, this->Pos()) < (double)80)
		{
			this->SetArrivePos(Vector2D(this->arrivePos.x, this->arrivePos.y - (double)20));
			this->World()->SetCrosshair(this->arrivePos);
		}
	}
	
}

void LeaderAgent::GoDown()
{
	if (this->userControl)
	{

		if (Vec2DDistance(this->arrivePos, this->Pos()) < (double)80)
		{
			this->SetArrivePos(Vector2D(this->arrivePos.x, this->arrivePos.y + (double)20));
			this->World()->SetCrosshair(this->arrivePos);
		}
	}
}

void LeaderAgent::GoLeft()
{
	if (this->userControl)
	{
		if (Vec2DDistance(this->arrivePos, this->Pos()) < (double)80)
		{
			this->SetArrivePos(Vector2D(this->arrivePos.x - (double)20, this->arrivePos.y));
			this->World()->SetCrosshair(this->arrivePos);
		}
	}
}

void LeaderAgent::GoRight()
{
	if (this->userControl)
	{
		if (Vec2DDistance(this->arrivePos, this->Pos()) < (double)80)
		{
			this->SetArrivePos(Vector2D(this->arrivePos.x + (double)20, this->arrivePos.y));
			this->World()->SetCrosshair(this->arrivePos);
		}
	}
}

//-------------------------------- Render -------------------------------------
//-----------------------------------------------------------------------------
void LeaderAgent::Render()
{
	//a vector to hold the transformed vertices
	static std::vector<Vector2D>  m_vecVehicleVBTrans;

	if (this->userControl)
	{
		gdi->GreenPen();
	}
	else
	{
		gdi->RedPen();
	}
	

	if (isSmoothingOn())
	{
		m_vecVehicleVBTrans = WorldTransform(m_vecVehicleVB,
			Pos(),
			SmoothedHeading(),
			SmoothedHeading().Perp(),
			Scale());
	}

	else
	{
		m_vecVehicleVBTrans = WorldTransform(m_vecVehicleVB,
			Pos(),
			Heading(),
			Side(),
			Scale());
	}


	gdi->ClosedShape(m_vecVehicleVBTrans);
	
	//render any visual aids / and or user options
	if (m_pWorld->ViewKeys())
	{
		Steering()->RenderAids();
	}
}